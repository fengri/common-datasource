/**
 * 
 */
package test.common.distributed.datasource.ibatis.service.impl;

import java.util.Map;

import com.common.distributed.datasource.spring.config.schema.CommonDatasourceSchema;

import test.common.distributed.datasource.ibatis.dao.IDemoDao;
import test.common.distributed.datasource.ibatis.model.DemoModel;
import test.common.distributed.datasource.ibatis.model.DemoParam;
import test.common.distributed.datasource.ibatis.service.IDemoService;

/**
 * @author liubing1
 *
 */
public class DemoServiceImpl implements IDemoService {
	
	private CommonDatasourceSchema commonDatasourceSchema;
	
	private IDemoDao demoDao;
	/* (non-Javadoc)
	 * @see test.jd.common.distributed.datasource.ibatis.service.IDemoService#doCreate(test.jd.common.distributed.datasource.ibatis.model.DemoModel)
	 */
	@Override
	public void doCreate(DemoParam demoParam,boolean flag) throws Exception {
		// TODO Auto-generated method stub
		getDemoDao().insert(demoParam);
		if(flag){
			Integer.parseInt("s");
		}
		
	}

	/* (non-Javadoc)
	 * @see test.jd.common.distributed.datasource.ibatis.service.IDemoService#doDelete(java.lang.Integer)
	 */
	@Override
	public void doDelete(Map<String, Object> params) throws Exception {
		// TODO Auto-generated method stub
		getDemoDao().delete(params);
	}

	/* (non-Javadoc)
	 * @see test.jd.common.distributed.datasource.ibatis.service.IDemoService#doUpdate(test.jd.common.distributed.datasource.ibatis.model.DemoModel)
	 */
	@Override
	public void doUpdate(DemoParam demoParam) throws Exception {
		// TODO Auto-generated method stub
		getDemoDao().update(demoParam);
	}

	/* (non-Javadoc)
	 * @see test.jd.common.distributed.datasource.ibatis.service.IDemoService#findById(java.util.Map)
	 */
	@Override
	public DemoModel findById(Map<String, Object> params) throws Exception {
		// TODO Auto-generated method stub
		return getDemoDao().findById(params);
	}

	/**
	 * @return the demoDao
	 */
	public IDemoDao getDemoDao() {
		return demoDao;
	}

	/**
	 * @param demoDao the demoDao to set
	 */
	public void setDemoDao(IDemoDao demoDao) {
		this.demoDao = demoDao;
	}
	
}
